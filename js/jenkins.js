var Jenkins = function(storage, $http, $log) {
	"use strict";

	var api = "/api/json";
	var options = {};

	this.BUILD_UNSTABLE = "UNSTABLE";
	this.BUILD_SUCCESSFULL = "SUCCESS";

	storage.onUpdate("options", function(){
		$log.info("Options changed");
		loadOptions();
	});
	
	this.getJenkinsInformation = function(callback) {
		var url = options.jenkins_url + api;
		url += "?tree=description";
		$http.get(url).success(function(data) {
			callback.success(data);
		});
		
	};
	
	this.getJobs = function(callback) {
		if (options) {
			var url = options.jenkins_url + api;
			url += "?tree=jobs[name,color,url]";
			$http.get(url).success(function(data) {
				callback.success(data.jobs);
			}).error(function(data)  {
				callback.error(data);
			});
		}
	};

	this.getLastBuild = function(jobname, callback) {
		var url = options.jenkins_url + "/job/" + jobname + api;
		url += "?depth=1&tree=lastBuild[building,fullDisplayName,number,result,url]";
		$http.get(url).success(function(data) {
			callback.success(data.lastBuild);
		}).error(function(data) {
			callback.success(data);
		});
	};
	
	var loadOptions = function() {
		storage.get("options", function(opt) {
			options = opt;
		});
	};
	
	loadOptions();
};
