"use strict";

var Notify = function(win, log, timeout) {

	var wn = win.webkitNotifications;

	var defaultOptions = {
		icon: "icon.png",
		timeout: 0,
	};

	this.PERMISSION_ALLOWED = 0;
	this.PERMISSION_NOT_ALLOWED = 1;
	this.PERMISSION_DENIED = 2;

	this.checkPermission = wn.checkPersmission;
	this.requestPermission = wn.requestPermission;

	this.createNotification = function(title, content, options) {
		var ops = angular.extend(defaultOptions, options);
		var nf = wn.createNotification(ops.icon, title, content);
		nf.show();
		if (ops.timeout > 0) {
			timeout(function(){
				nf.cancel();
			}, ops.timeout);
		}
	};
};

angular.module("angular-notify", []).
	config(function($provide) {
		$provide.factory("notify", ["$window", "$log", "$timeout", function(win, log, timeout) {
			if (!win.webkitNotifications) {
				log.log("Desktop notifications are not supported!");
				return;
			}
			return new Notify(win, log, timeout);
		}]);
	});
