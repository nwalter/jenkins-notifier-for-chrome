/**
 * Background JavaScript
 */
function backgroundControl($scope, log, jenkins, storage, $timeout, notify) {
	"use strict";

	var options;
	var builds = {};

	$scope.onInit = function () {
		storage.get("options", function (opt) {
			options = opt;
			checkServer();
		});
		storage.onUpdate("options", function (key, opt) {
			if (key === "options") {
				options = opt;
			}
		});
	};
	
	var scheduleCheck = function () {
		$timeout(checkServer, options.interval * 1000);
	};
	
	var checkServer = function () {
		angular.forEach(options.jobs, function (value, key) {
			jenkins.getLastBuild(key, {
				success : function (lastBuild) {
					checkBuild(key, lastBuild);
				},
				error : function (error) {
					log.error(error);
				}
			});
		});

		scheduleCheck();
	};

	var checkBuild = function (job, lastBuild) {
		var prevBuild = builds[job];
		if (prevBuild) {
			if (prevBuild.number !== lastBuild.number) {
				newBuild(job, lastBuild);
			}
			else if (prevBuild.result !== lastBuild.result) {
				buildUpdated(job, lastBuild);
			}
		} else {
			if (!lastBuild.result) {
				newBuild(job, lastBuild);
			}
		}

		builds[job] = lastBuild;
	};

	var newBuild = function (job, build) {
		var title = "[" + job + "] started";
		var content = build.fullDisplayName;
		var icon = "img/icon.png";

		showNotification(title, content, icon);
	};

	var buildUpdated = function (job, build) {
		var title = "[" + job + "] finished";
		var content = build.fullDisplayName + "\nResult: " + build.result;
		var icon = getIconPath(build.result);

		showNotification(title, content, icon);
	};

	var showNotification = function (title, content, icon) {


		notify.createNotification(title, content, {
			"timeout" : 5000,
			"icon" : icon
		});
	};

	var getIconPath = function (result) {
		var img = "img/";
		if (result === jenkins.BUILD_UNSTABLE) {
			return img + "yellow.png";
		}
		if (result === jenkins.BUILD_SUCCESSFULL) {
			return img + "blue.png";
		}
	};
}
